// GLOBAL
function DOM_ID(id) {
  return document.getElementById(id);
}

// BÀI 1
document.querySelector("#v-pills-bai1 button").onclick = function () {
  var tong = 0;
  for (var i = 1; ; i++) {
    tong += i;
    if (tong > 10000) {
      DOM_ID("resultBT1").innerText = i;
      return;
    }
  }
};

// BÀI 2
document.querySelector("#v-pills-bai2 button").onclick = function () {
  soX = Number(DOM_ID("soX_BT2").value);
  soN = Number(DOM_ID("soN_BT2").value);
  var tong = 0;
  for (var i = 1; i <= soN; i++) {
    tong += Math.pow(soX, i);
  }
  DOM_ID("resultBT2").innerText = tong;
};

// BÀI 3
document.querySelector("#v-pills-bai3 button").onclick = function () {
  soN = Number(DOM_ID("soN_BT3").value);
  var result = 1;
  for (var i = 1; i <= soN; i++) {
    result *= i;
  }
  DOM_ID("resultBT3").innerText = result;
};

// BÀI 4
document.querySelector("#v-pills-bai4 button").onclick = function () {
  var result = "";
  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      result += `<div style="background-color: red; padding: 0.25rem">Div chẵn ${i}</div>`;
    } else {
      result += `<div style="background-color: blue; padding: 0.25rem">Div lẻ ${i}</div>`;
    }
  }
  DOM_ID("resultBT4").innerHTML = result;
};
